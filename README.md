# **[JourneyMap for Minecraft](http://journeymap.info)** Language Files

Language localization resources for the [JourneyMap]([http://journeymap.info) mod for Minecraft.

## How to translate JourneyMap

* See **[Issues](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on)** for a list of all current translation needs
* Addition of new languages is welcome!
* **Very Important**: Please read the [How to translate JourneyMap](http://journeymap.techbrew.net/help/wiki/Translate_JourneyMap_For_Your_Language) wiki page for detailed instructions.

## How to submit your translation (Without using Git)

1. [Sign Up](https://bitbucket.org/account/signup/) for a free account on BitBucket.org
1. Browse the journeymap-lang [Source files](https://bitbucket.org/TeamJM/journeymap-lang/src) to the file you wish to change
1. Changes to en_US files are generally not accepted unless they address typos.
1. Press the **Edit** button at the top of the file view.
1. Change the settings below the file editor to match the file type: (.lang = "Properties files") (.json = "JSON")
1. When your changes are complete, **Commit** your changes and provide a message explaining what you did. If your changes relate to a particular [Issue](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on), please note the Issue # in your message.
1. A new Pull Request (PR) will be created and reviewed by a member of TeamJM.  If approved, it will be merged into the official source files.

## How to submit your translation (Using Git)
 
1. [Sign Up](https://bitbucket.org/account/signup/) for a free account on BitBucket.org
1. Fork this repository with a Git tool, Source Tree, etc.
1. Update/add files as needed.
1. Changes to en_US files are generally not accepted unless they address typos. 
1. [Open a new Pull Request](https://bitbucket.org/TeamJM/journeymap-lang/pull-requests/new) (PR). If your changes relate to a particular [Issue](https://bitbucket.org/TeamJM/journeymap-lang/issues?status=new&status=open&sort=created_on), please note the Issue # in your PR title.
1. If you see any problems with display of the translated messages, please make a note of it in the description of the PR. For example, there may not be enough space provided to display a certain phrase. In that case, please note the message key and describe the problem you see.
1. Your new Pull Request (PR) will reviewed by a member of TeamJM.  If approved, it will be merged into the official source files.

## Thanks for your help!